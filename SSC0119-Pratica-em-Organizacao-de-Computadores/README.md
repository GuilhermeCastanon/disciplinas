# SSC0119-Pratica-em-Organizacao-de-Computadores
Disciplina SSC0119 Prática em Organização de Computadores

- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@icmc.usp.br  (Obs.: só tem um @)
- Departamento de Sistemas de Computação – ICMC - USP
- Grupo de Sistemas Embarcados e Evolutivos
- Laboratório de Computação Reconfigurável

## Alunos de 2022 - Primeiro semestre
- Lista de Presença - Favor assinar a Lista de Presença Impressa durante o horario das aulas
- Essa disciplina irá utilizar o projeto do Processador ICMC, que é um processador RISC de 16 bits implementado em FPGA - todas as ferramentas de software e hardware estão disponíveis no Github do projeto: https://github.com/simoesusp/Processador-ICMC


## Processador ICMC - https://github.com/simoesusp/Processador-ICMC

- Nossa disciplina irá usar um Processador desenvolvido pelos próprios alunos do ICMC disponível neste repositório do github

## Desenho do Processador ICMC no FIGMA
- Link para visualisar o Desenho na Ferramenta FIGMA: https://www.figma.com/file/mHQmhfvLiwmIRXkHlFDTyn/CPU_ICMC-team-library?node-id=415%3A589

## Simulador para programação em Assembly 
- Nossa disciplina irá usar um simulador para desenvolver programas em linguagem Assembly que poderá ser encontrado para Windows, Linux e MacOS em: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/
- Para Windows, fiz um link super fácil de instalar: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/Simulador_Windows_Tudo_Pronto_F%C3%A1cil%20(1).zip
  - Esse zip já vem inclusive com o sublime configurado para escrever o software (incluindo a sintaxe highlight) e o montador e o simulador já configurado para ser chamado com a tecla F7
  - Para instalar basta fazer o download na area de trabalho ou na pasta Documentos
  - Entrar na pasta ..\Simulador\Sublime Text 3
  - Executar o sublime: "sublime_text.exe"
  - Se ele pedir, NÃ0 FAÇA O UPDATE !!!!!!!!!!!!!!!
  - Vá em File - Open File e volte uma pasta para ..\Simulador\
  - Abra o sw em Assembly chamado Hello4.ASM
  - Teste se está tudo funcionando chamando o MONTADOR e o SIMULADOR com a tecla F7
  - Apartir daí pode-se salvar o sw com outro nome e fazer novos programas
  - Apenas preste atenção para estar na pasta ..\Simulador\
  - Se der o erro: [Decode error - output not utf-8] é porque você não está na pasta ..\Simulador\
   - ... Ou basta mudar o formato para utf-8 e salvar...



## Avaliação
- A avaliação será por meio da apresentação de um trabalho - Implementação de um Jogo em Linguagem Montadora (assembly) no processador ICMC (https://github.com/simoesusp/Processador-ICMC/tree/master/Processor_FPGA)
- Os alunos serão divididos em grupos de 2-3 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

### Apresentação do trabalho Turma 2022 - Primeiro semestre:
- Apresentação dos trabalhos será presencial no Laboratório e deve ser agendada para as 2 ultimas semanas de aula no link abaixo:

- Insira seu projeto e reserve um horario para apresentar no arquivo do GoogleDocs (precisa estar logado na USP), informando: TÍTULO DO PROJETO - nomes dos alunos - link pro github/gitlab - LINK DOCS: https://docs.google.com/spreadsheets/d/1YLnMBhXu-unA0WvrD2lRk95P10cuRBF0qPKFX1um2CY/edit?usp=sharing

## Trabalho da Disciplina:
- 1) Implementar um Jogo em Assembly
- 2) Construir o Hardware do Processador em VHDL na FPGA
- 3) Propor uma modificação na Arquitetura do Procassador(uma nova Instrução, por exemplo) e implementá-la em VHDL na FPGA
     - Para isso, deve-se modificar o projeto na FPGA
     - Modificar o Montador para que reconheça sua nova Instrução (exemplo - [Link](https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0119-Pratica-em-Organizacao-de-Computadores/Modificar_Montador/README.md))
     - Modificar o Manual do Processador, descrvendo sua nova Instrução (e alteraçao na Arquitetura)
- 4) OPCIONAL: reescrever o código do Jogo para incorporar a modificação no Hardware e testar no "OpenGL_Simulator" ou na FPGA



# Projetos dos Alunos 2022
- Criar uma conta sua no Github/gitlab
- Os projetos devem conter um Readme explicando o projeto e o software deve estar muito bem comentado!!
- Incluir no seu Github/gitlab: o projeto na FPGA do Processador modificado, o JOGO.ASM e o CPURAM.MIF, e caso tenha alterado, o CHARMAP.MIF. 
- Obrigatório: incluir um vídeo mostrando o jogo funcionando e explicando as modificações que fez no Processador. (Upa no Youtube/GoogleDrive e poe um link no Readme do teu Github/guitlab).
- Além do VÍDEO DE VOCË explicando o projeto, TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE no dia e horario agendado

## Minicurso OpenGL do Breno - https://github.com/Brenocq/OpenGL-Tutorial


# Aulas a Distância (2021 - Primeiro Semestre)

- O Material usado nas aulas está na pasta MaterialAulaDistancia

- Aula01 16/04/2021 (16:20h) - https://drive.google.com/file/d/1Bgw7v4G2tcryxD6RC8bhKQfjIoB7jpKu/view?usp=sharing

- Aula02 23/04/2021 (16:20h) - https://drive.google.com/file/d/17kQHaB83eE9A-oUNBYiPEWSCe-TGzH0F/view?usp=sharing

- Aula03 30/04/2021 (16:20h) - https://drive.google.com/file/d/1JW5gZ4LY6g-pu0K_WHYNMj-AMJrH00y2/view?usp=sharing

- Aula04 07/05/2021 (16:20h) - https://drive.google.com/file/d/1FX5-CeRvyuZwyf0XcUx6uek84lua1TvW/view?usp=sharing

- Aula05 14/05/2021 (16:20h) - https://drive.google.com/file/d/1Xhit1h19txz2XjqDHVJBHgM_d3N54a2B/view?usp=sharing
  - Explicação muito simples sobre JMP e BRANCH - https://en.wikibooks.org/wiki/Microprocessor_Design/Program_Counter

- Aula06 21/05/2021 (16:20h) - https://drive.google.com/file/d/1TrbT_Sef5_9rWwrpavi8hfBN83UMoyR-/view?usp=sharing

- Aula07 28/05/2021 (16:20h) - https://drive.google.com/file/d/1eatRDyZWmGQD4X9kU_51daoYY3k2DFyC/view?usp=sharing

- Aula08 11/06/2021 (16:20h) - https://drive.google.com/file/d/1ftWsAGOG_uY9l64u7407kzICY-u7Ybjg/view?usp=sharing

- Aula09 18/06/2021 (16:20h) - https://drive.google.com/file/d/1YjKzjqStwjoHxIE-UtzQX7TN5EqnMrPG/view?usp=sharing

- Aula10 25/06/2021 (16:20h) - https://drive.google.com/file/d/1-ROJ5zd_Q7RW9JMTMpDJBfYvYnBR_hvu/view?usp=sharing

- Aula11 02/07/2021 (16:20h) - https://drive.google.com/file/d/1z-l5Y1GSaMI5NOEYTJ6G2U0CF1QZd78r/view?usp=sharing

- Aula12 16/07/2021 (16:20h) - https://drive.google.com/file/d/14ZiytkSHrAstb9RCMEou6-iPvk4WH4sO/view?usp=sharing

- Aula13 23/07/2021 (16:20h) - https://drive.google.com/file/d/11XQJg0GTxt1rD-U3pm_W6x2txzQuIl-z/view?usp=sharing

- Aula14 30/07/2021 (16:20h) - Link para o Google Meet - meet.google.com/get-ygkf-zgz

## Aulas a Distância 2020 - Primeiro Semestre

- Minicurso OpenGL do Breno - https://github.com/Brenocq/OpenGL-Tutorial

- Aula01 - https://drive.google.com/file/d/1ucFPXroi7lXQq5qj4DN_a59JgJtiMLZR/view?usp=sharing

- Aula02 - https://drive.google.com/file/d/1okiL76FewUbyGMTcjNSI0Xhy0rsizia2/view?usp=sharing

- Aula03 17/04/20 - Parte1: https://drive.google.com/file/d/1JugSodvm6wiZIyiCYXl-gaDbAdB2Yd1P/view?usp=sharing - 
Parte2: https://drive.google.com/file/d/15Olmivl_6dFfzbz0j2-lQUj2PCYKGOXX/view?usp=sharing

- Aula04 24/04/20 - https://drive.google.com/file/d/1UgvGZ4suo8emRTWnyZTSAoGBYFHCAPB-/view?usp=sharing

- Aula05 08/05/20 - https://drive.google.com/file/d/1aMhA7_8POKLhRMsvV9SxC-DVYDXbCiBP/view?usp=sharing

- Aula06 15/05/20 - https://drive.google.com/file/d/1V-i44oPmlpRQ5K5WAYF6ywZH5auTft93/view?usp=sharing

- Aula07 20/05/20 - https://drive.google.com/file/d/1DRlHb2YtotdG1Rkqs8OJSG8z4QznJbyy/view?usp=sharing

- Aula08 29/05/20 - https://drive.google.com/file/d/10DAdvf9fJx04aoz0vpfWeKK0oT7K8vpt/view?usp=sharing

- Aula09 05/06/20 - https://drive.google.com/file/d/1D737QeewEPxa9iTV7XARUwV22v9Pwsif/view?usp=sharing

- Aula10 19/06/20 - https://drive.google.com/file/d/117qgL7HN25YzItavkQm8LA9Z57q-cyQC/view?usp=sharing

- Aula11 26/06/20 - https://drive.google.com/file/d/1hCCEEEl75a0nuwn8l52oTFqBOmnQoUKe/view?usp=sharing

- Aula12 03/07/20 - https://drive.google.com/file/d/1RzOF8D1m6F4eZPwDyDH26HVcAFpasZUJ/view?usp=sharing

- Aula13 10/07/20 - https://drive.google.com/file/d/1HX1UmjykazWGOtTLt3_q1knYkVYag2FU/view?usp=sharing

- Aula14 17/07/20 - https://drive.google.com/file/d/1eWgq85L6G6-anK1W4bojcdzM84GF_xvi/view?usp=sharing

- Aula15 24/07/20 - https://drive.google.com/file/d/1VYH5Ttz9j9WaDt0n5hL2UDnE31Iir3x-/view?usp=sharing

# Projetos dos Alunos 2021

- Grupo 1 - Teris Evolutivo - Tiago Triques, Ricardo Araujo -  https://gitlab.com/tiagot/teris-evolutivo

- Grupo 2 - Type The Game - Matheus Ventura de Sousa, Luiz Fernando Rabelo - https://github.com/matheus-sousa007/TypeTheGameAssembly

- Grupo 3 - Genius Assembly  - Milena Corrêa da Silva, Guilherme Lourenço de Toledo, João Victor Sene Araujo - https://github.com/milenacsilva/genius-assembly

- Grupo 4 - Campo Minado (Assembly Minado) - Giovanni Shibaki Camargo, Lucas Keiti Anbo Mihara, Pedro Kenzo Muramatsu Carmo - https://github.com/giovanni-shibaki/SSC0119_Jogo_Assembler

- Grupo 5 - HanoiAsm - Marco Antônio Ribeiro de Toledo, Vitor Caetano Brustolin: https://github.com/Ocramoi/HanoiAsm

- Grupo 6 - Tomb of the Mask - Gabriel Alves Kuabara, Lourenço de Salles Roselino, Gabriel Victor Cardoso Fernandes: https://github.com/GKuabara/tomb-of-the-mask

- Grupo 7 - MemóriaAssembly - Pedro Henrique Borges Monici, Rodrigo Lopes Assaf, Diógenes Silva Pedro: https://github.com/pedromonici/MemoriaAssembly

- Grupo 8 - RISC Zero - Gabriel da Cunha Dertoni, Natan Henrique Sanches, João Francisco Caprioli Barbosa Camargo de Pinho: https://github.com/GabrielDertoni/risc_zero

- Grupo 9 - Forca 2 jogadores - Pedro Henrique Raymundi, Gabriel Fachini, Victor Lima: https://github.com/PedroRaymundi/Forca_2_jogadores_assembly

- Grupo 10 - Jogo da Velha - Matheus Henrique de Cerqueira Pinto, Gustavo Henrique Brunelli, Pedro Lucas de Moliner de Castro: https://github.com/CerqueiraMatheus/SSC0119-Projeto-Final

- Grupo 11 - Pong - Gabriel Vicente Rodrigues, João Pedro Gavassa Favoretti, Lucas Pilla Pimentel, Ciro Grossi Falsarella: https://github.com/gabriel-vr/PingPongAssemblyICMC 

- Grupo 12 - Sistema Operacional - Ariel de Oliveira Cardoso:
https://github.com/aocard/AOC_ICMC-OS

- Grupo 13 - Snake - Ana Cristina Silva de Oliveira, Gustavo de Oliveira Silva, Gustavo Lelli Guirao
momoyo-droid/snake-assembly: [GRAD] Jogo Snake feito para a disciplina de Prática em Organização de Computadores. (github.com)

- Grupo 14 - Jogo da memória - Erick Patrick Andrade Barcelos, Vinicius Santos Monteiro, Yann Amado Nunes Costa
https://github.com/brcls/Jogo-da-Memoria-Assembly

- Grupo 15 - Breakout - Marcos Almeida, Beatriz Diniz, Tulio Santana Ramos, Victor Paulo Cruz Lutes: https://github.com/VictorLutes/BreakoutPraticaOrgComp 

- Grupo 16 - Tower Defense Digitação - Sofhia de Souza Gonçalves, Matheus Luis Oliveira da Silva e Guilherme Machado Rios

- Grupo 17 - Flappy Bird - Guilherme Ramos Costa Paixão, Filipe Augusto Oliveira da Costa e Marcus Vinícius Teixeira Huziwara: https://github.com/gp2112/FlappyBirdAssembly 

- Grupo 18 - Pega Bandeira - Érica Ribeiro Filgueira dos Santos: https://github.com/ericarfs/Pega-Bandeira-Assembly



## Projetos dos Alunos 2020

- Processador do Breno Cunha Queiroz: https://github.com/Brenocq/MyMachine

-  Assembly Tron - Guilherme Amaral Hiromoto, Paulo Matana da Rocha, Caio Marcos Chaves Viana, Dennis Lenke Green - https://github.com/guilhermehiromoto/Assembly-Tron

- Pacmario - Luana Terra do Couto, Larissa freire de Jesus Costa - https://github.com/LuTDC/lab_orgcomp

- Atravesse o Mapa - Luca Porto, Leonardo Meireles - https://github.com/LucaPort0/Assembly-ICMC-Game

- Space Hit - Isadora Carolina Siebert, Marlon José Martins - https://github.com/mjmartins11/SpaceHit-Assembly

- Frogger - David Souza, Gabriel Toschi, Matheus Alves - https://github.com/bicanco/SSC0119-2020-1

- RNAssembly - Anderson Cardoso Gonçalves - https://github.com/ansoncg/RNAssembly_LabOrgComp

- HamsterDuet - Diany Pressato, Matheus da Silva Araujo - https://github.com/di-press/HamsterDuet

- Processador MASM - Eduardo Souza Rocha - https://github.com/Edwolt/Processador-MASM

- Processador de Videogame - Nathan Rodrigues de Oliveira - https://github.com/NathanTBP/NTBProcessor

- Space Race - Tyago Yuji Teoi - https://github.com/Tyago-Teoi/Projeto-de-Pr-ticas-de-Organiza-o-de-Computadores



## Projetos dos Alunos 2019

- Jogo da chuva de letras: Felipe Guilermmo Santuche Moleiro - https://github.com/FelipeMoleiro/ProjetoLabOrgArqComp

- Rex Scape: Caio Augusto Duarte Basso, Gabriel Garcia Lorencetti, Giovana Daniele da Silva, Luana Balador Belisario - https://github.com/gabrielgarcia7/game-rexscape

- Space Laser: Gustavo Vinicius Vieira Silva Soares - https://github.com/gsoares1928/ProjLabOrgArq

- Labyrinth Snake: Julia Carolina Frare Peixoto, Luís Eduardo Rozante de Freitas Pereira, Maurílio da Motta Meireles - https://github.com/LuisEduardoR/Processador-ICMC

- Snake: Gabriel Mattheus Bezerra Alves de Carvalho, Wallace Cruz de Souza - https://github.com/GabrielBCarvalho/Snake-Assembly

- Pencil: Marcelo Isaias de Moraes Junior - https://github.com/MarceloMoraesJr/JogoOrgComp

- Battle of Tiers: Jonas Wendel Costa de Souza - https://github.com/4Vertrigo/BattleOfTiers

- Bandersnack - A Dora's Text Adventure: Igor Lovatto Resende, Lucas Mateus M. A. Castro, Marcelo Temoteo de Castro - https://github.com/Beadurof/dora-machineworks

- Tic-Tac-Toe-Assembly-ICMC: NOMES: Guilherme Holanda Sanches, Lucas Henrique Rodrigues, João Vitor VIllaça - https://github.com/holondo/Tic-Tac-Toe-Assembly-ICMC
